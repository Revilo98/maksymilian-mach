//============================================================================
// Name        : Nazwa.cpp
// Author      : Maksymilian Mach
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <string>
#include <iostream>
#include <fstream>
#include <math.h>
#include <random>
#include <cmath>
#include <array>
#include <stdexcept>
#include "ffft/FFTReal.h"

using namespace std;

ofstream plik;
class Packet {
protected:
	string device;
	string description;
	long date;

public:
	Packet(string Urzadzenie, string Opis, long Data):
		device(Urzadzenie), description(Opis), date(Data)
		{};

	virtual void ToString(){
		cout<<"Nazwa Urzadzenia: "<<device<<endl;
		cout<<"Opis danych: "<<description<<endl;
		cout<<"Data(ilosc sekund od poczatku czasu): "<<date<<endl;
	}

	virtual void sth() = 0;
	virtual ~Packet() {};
};


template <class T, int X=8>
class Sequence : public Packet{
protected:
	int channelNr;
	string unit;
	double resolution;
	T *buffer= new T[X];

			public:

				Sequence<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc): Packet(Urzadzenie, Opis, Data), channelNr(Kanal), unit(Jednostka), resolution(Rozdzielczosc) {};

					virtual void ToString(void){
						Packet::ToString();
						cout<<"Numer Kanalu: "<<channelNr<<endl;
						cout<<"Jednostka: "<<unit<<endl;
						cout<<"Rozdzielczosc: "<<resolution<<endl;
						cout<<"Tablica danych typu T: "<<endl;
				for(int i=0; i<X; i++)
				cout<<buffer[i]<<"\t";
				cout<<endl;
		};
	virtual ~Sequence<T,X>() {};

};

template <class T, int X=8>
	class Spectrum : public Sequence<T,X>{
		protected:
			int scaling;
			string scala;

		public:
			Spectrum<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc, int Skala):Sequence<T,X>(Urzadzenie, Opis, Data, Kanal, Jednostka, Rozdzielczosc), scaling(Skala) {};
				void ToString(){
					Sequence<T,X>::toString();
					cout << "Skala: " << scala << endl;
					cout<<endl;
	}

};

template <class T, int X=8>
	class TimeHistory : public Sequence<T,X>{
		protected:
			double sensitivity;
			double srednia, Rms, odchylenie;
			T  buffer[X];
			float real[X], imag[X], modul[X];
		public:
			TimeHistory<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc, double Czulosc, double srednia, double odchylenie):Sequence<T,X>(Urzadzenie, Opis, Data,Kanal, Jednostka, Rozdzielczosc), sensitivity(Czulosc), srednia(srednia), odchylenie(odchylenie) {};
				void Gauss(){
					std::random_device rand{};
					std::mt19937 gen{rand()};
					std::normal_distribution<> distribution{srednia,odchylenie};
					try{
		    	for (int i=0; i<X; ++i){
		    	buffer[i]=distribution(gen);
		    	}
		    }
		    catch(int  i){
		    	cerr << "zly indeks" << i << endl;
		    }
		    catch(...){
		    	cerr << "nieznany wyjatek";
		    }
	}
	virtual	~TimeHistory<T,X>() {};

	void RMS(){
		double r=0;
		double rms[X];
	    for (int i=0; i<X; ++i){
	    	rms[i]=pow(buffer[i],2);
	    	r += rms[i];
	    }
	    Rms = sqrt(r/X);
		cout << "RMS: " << Rms << endl;
	}

			void FFT(int z){
				float f[8];
				ffft::FFTReal<float> fftObject(8);
				fftObject.do_fft(f,&buffer[0]);

				for(int i=0; i< X/2; i++){
					real[i] = f[i];
				}
				for(int i=1; i<= X/2; i++){
					real[X-i] = f[i];
				}
				imag[0]=0.0;
				imag[X/2]=0.0;
					for(int i=(X/2)+1; i<X; i++){
					imag[i-X/2]=f[i];
					}
					for(int i=(2)+1; i<=X/2; i++){
						imag[i+X/2-1]=f[X-i+1];
					}
					for(int i=0;i<X;i++){
			modul[i]=sqrt(imag[i]*imag[i]+real[i]*real[i]);
					}
					switch(z){
					case 1:
						cout << "FFT widmo zespolone:" << endl;
						for (int i=0; i<X; i++){
							cout << real[i] << " +(";
							cout << imag[i] << ")i\t";
						}
						cout << endl;
			break;
		case 2:
			cout << "FFT widmo amplitudowe:" << endl;
			for (int i=0; i<X; i++){
				cout << modul[i] << "\t";
			}
			cout << endl;
			break;
		default:
			cout << "1 - widmo zespolone, 2 - widmo amplitudowe" << endl;
			break;
		}
	}

	void ToString() {
		Sequence<T,X>::ToString();
		cout << "Czulosc: " << sensitivity << endl;
		cout << "Srednia: " << srednia << endl;
		cout << "Odchylenie: " << odchylenie << endl;
		cout << "Tablica wzmocnien: " << buffer << endl;
			for (int i=0; i<X; i++){
				cout << buffer[i] << "\t";
			}
		cout << endl;
	}

	void operator=(const TimeHistory<T,X>& r){
		 for (int i=0; i<X; ++i){
			buffer[i] = r.buffer[i];
		  }
	}

	void operator+(const TimeHistory<T,X>& r){
		TimeHistory<T,X> tmp("Urzadzenie","Dodawanie", 111, 5, "Sekundy", 0, 0, 0, 0);
		for (int i=0; i<X; ++i){
			tmp.buffer[i] = buffer[i]+r.buffer[i];
		  }
		tmp.ToString();
	}

		void operator/(const TimeHistory<T,X>& r){
			TimeHistory<T,X> tmp("Urzadzenie","Dzielenie", 111, 6, "Sekundy", 0, 0, 0, 0);
			for (int i=0; i<X; ++i){
				tmp.buffer[i] = buffer[i]/r.buffer[i];
				if (r.buffer[i]==0){
					string wyjatek = "Dzielenie przez zero";
					throw wyjatek;
				}
			}
			tmp.ToString();
	}

		void write(){
			if(plik.is_open()){
				for(int i=0; i<X;i++){
					plik << buffer[i] << " ";
				}
				plik << endl;
				for (int i=0; i<X; i++){
			plik << real[i] << " +(";
			plik << imag[i] << ")i\t";
		}
		plik << endl;
		for(int i=0; i<X;i++){
			plik << modul[i] << " ";
		}
		plik << endl << endl;
		}
		else{
			string wyjatek = "Plik nie zostal otwarty";
			throw wyjatek;
		}

	}

	void sth() {};
};


class Alarm : public Packet{
	protected:
		int channelNr;
		double threshold;
		int direction;
		string kierunek;

	public:
		Alarm(string Urzadzenie, string Opis, long Data, int Kanal, double Przekroczenie, int Kierunek): Packet(Urzadzenie, Opis, Data), channelNr(Kanal), threshold(Przekroczenie), direction(Kierunek) {};

	void toString(){
    	WyborKierunku();
 		 Packet::ToString();
 		cout<<"Numer Kanalu: "<<channelNr<<endl;
 		cout<<"Wartosc, ktorej przekroczenie powoduje sygnalizacje alarmu/alertu: "<<threshold<<endl;
		cout<<"Kieruenk zmiany: "<<kierunek<<endl;
		cout<<endl;
	}

		virtual ~Alarm() {};


		void sth() {};

		void WyborKierunku(){
			switch(direction){
			case -1:
			kierunek = "dol";
				break;
				case 0:
			kierunek = "r�ny";
				break;
				case 1:
			kierunek = "g�ra";
				break;
		default:
			kierunek = "-1 - dol, 0 - rozny,  1 - gora" ;
			break;
		}
 	 }
};

int main() {

try{
	plik.open("wyniki.txt");

		cout << "Test" << endl;

		cout << "TimeHistory:" << endl;
try{
TimeHistory<float,50> kanal1("Urzadzenie 1","Sygnal 1", 30, 1, "Sekundy", 100, 1, 3, 0.3),
						kanal2("Urzadzenie 2","Sygnal 2", 100, 2, "Sekundy", 15, 5, 6, 0.3),
						kanal3("Urzadzenie 3","Sygnal 3", 50, 3, "Sekundy", 10, 7, 8, 0.1),
						kanal4("Urzadzenie 4","Sygnal 4", 150, 4, "Sekundy", 20, 9, 3, 0.3);

kanal1.Gauss();
cout << "kanal1:" << endl;
kanal1.ToString();
kanal1.RMS();
kanal1.FFT(1);
kanal1.FFT(2);

kanal2.Gauss();
cout << "kanal2:" << endl;
kanal2.ToString();
kanal2.RMS();
kanal2.FFT(1);
kanal2.FFT(2);


kanal3.Gauss();
cout << "kanal3:" << endl;
kanal3.ToString();
kanal3.RMS();
kanal3.FFT(1);
kanal3.FFT(2);

kanal4.Gauss();
cout << "kanal4:" << endl;
kanal4.ToString();
kanal4.RMS();
kanal4.FFT(1);
kanal4.FFT(2);

kanal1=kanal2;
cout << "kanal1=kanal2:" << endl;
kanal1.ToString();

cout << "kanal2+kanal3:" << endl;
kanal2+kanal3;

cout << "kanal1/kanal4:" << endl;
try
	{
		kanal1/kanal4;
	}
		catch(string w)
		{
			cout << "Wyjatek: " << w;
		}

try
	{
		kanal1.write();
		kanal2.write();
		kanal3.write();
		kanal4.write();
	}
catch(string w){
		cout << "Wyjatek: " << w;
		plik.close();
	};
}
catch (std::runtime_error& e)
  {
    cout << "Wyjatek: " << e.what();
  }
}
catch(std::bad_alloc& ba)
{
	cout << "Wyjatek: " << ba.what();
}
	return 0;
	}

