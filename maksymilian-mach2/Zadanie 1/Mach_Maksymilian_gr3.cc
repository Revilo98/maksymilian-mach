#include <iostream>
#include <math.h>
using namespace std;

class Packet
{
    protected:
    string device;
    string description;
    long date;

    public:
        Packet(string Urzadzenie, string Opis, long Data) : device(Urzadzenie), description(Opis), date(Data)
            {

            };

                virtual void Abstrakcja()=0;
                virtual void toString(void)
                    {
                    cout<<"Nazwa Urzadzenia:"<<device<<endl;
                    cout<<"Opis danych:"<<description<<endl;
                    cout<<"Data(ilosc sekund od poczatku czasu):"<<date<<endl;
                    }
                virtual ~Packet()
        {

        };
};
template<class T, int X=8>
    class Sequence : public Packet
            {
            protected:
                int channelNr;
                string unit;
                double resolution;
                T*buffer=new T[X];

            public:
                Sequence<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc, T* Tablica) : Packet(Urzadzenie, Opis, Data), channelNr(Kanal), unit(Jednostka), resolution(Rozdzielczosc), buffer(Tablica)
                    {

                    };
                    virtual void toString(void)
                    {
                    Packet::toString();
                        cout<<"Numer Kanalu:"<<channelNr<<endl;
                        cout<<"Jednostka:"<<unit<<endl;
                        cout<<"Rozdzielczosc:"<<resolution<<endl;
                        cout<<"Tablica danych typu T:"<<buffer<<endl;
                            for(int i=0; i<X; i++)
                                cout<<buffer[i]<<"\t";
                    };
            virtual ~Sequence<T,X>()
            {

            };
        };
template <class T, int X=8>
    class TimeHistory: public Sequence<T,X>
    {
        private:
            double sensitivity;
        public:
            TimeHistory<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc, T Tablica[X], double Czulosc):Sequence<T,X>(Urzadzenie, Opis, Data, Kanal, Jednostka, Rozdzielczosc, Tablica), sensitivity(Czulosc)
            {

            };
            virtual void Abstrakcja() override{};
            void toString(void){
                    Sequence<T,X>::toString();
                        cout<<"Czulosc napieciowa:"<<sensitivity<<endl;
            }
        virtual ~TimeHistory<T,X>() {};
};

template <class T, int X=8>
    class Spectrum: public Sequence<T,X> {
        private:
            string scaling;
        public:
            Spectrum<T,X>(string Urzadzenie, string Opis, long Data, int Kanal, string Jednostka, double Rozdzielczosc, T Tablica[X], string Skala):Sequence<T,X>(Urzadzenie, Opis, Data, Kanal, Jednostka, Rozdzielczosc, Tablica), scaling(Skala)
            {

            };
            virtual void Abstrakcja() override{};
            void toString(void){
                    Sequence<T,X>::toString();
                        cout<<"Skala:"<<scaling<<endl;
		}
	virtual ~Spectrum<T,X>() {};
};

class Alarm : public Packet{
    private:
        int channelNr;
        double threshold;
        int direction;
    public:
        Alarm(string Urzadzenie, string Opis, long Data, int Kanal, double Przekrocznie, int Kierunek): Packet(Urzadzenie, Opis, Data), channelNr(Kanal),threshold(Przekrocznie),direction(Kierunek)
        {

        };
            virtual void Abstrakcja() override{};
            void toString(void){
                Packet::toString();
                    cout<<"Numer Kanalu:"<<channelNr<<endl;
                    cout<<"Wartosc, ktorej przekroczenie powoduje sygnalizacje alarmu/alertu:"<<threshold<<endl;
                    cout<<"Kierunek zmiany:"<<direction<<endl;
			}
};
int main() {
    int Tablica1[10]={1,2,3,4,5,6,7,8,9,10};

            TimeHistory<int, 10> a(" Monitoring nr 1"," Przebieg czasowy",5294672,1," sekunda",1024,Tablica1,20);
            a.toString();

	int Tablica2[8]={1,2,3,4,5,6,7,8};
            Spectrum<int, 8> b(" Monitoring nr 2"," Widmo amplitudowe",1111222,2," milisekunda",1600,Tablica2,"liniowa");
            b.toString();

            Alarm c(" Monitoring nr 3"," Alarm lub alert",6674465,1,0.2,-1);
            c.toString();

	return 0;
}


